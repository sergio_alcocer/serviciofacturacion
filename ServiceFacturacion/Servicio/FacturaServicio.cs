﻿using ServiceFacturacion.Datos;
using ServiceFacturacion.Negocio;
using ServiceFacturacion.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ServiceFacturacion.Servicio
{
    public class FacturaServicio
    {
        string Resultado_Facturacion;
        string mensaje;
        int Bitacora_Facturacion_ID;
        private Timer _timer;
        public bool BlnEnProceso = false;
        public string CicloFacturar = "";
        public DataTable Dt_Predios_Facturacion = new DataTable();
        public int Year;
        public int Month;
        public Bitacora_Facturacion_Datos bitacora_Facturacion_Datos = new Bitacora_Facturacion_Datos();

        public FacturaServicio() {

            // 900000 = 15 minutos     // 600000 = 10 minutos  //  1200000 = 20 minutos // 3600000 = 60 minutos
            _timer = new Timer(3600000) { AutoReset = true };
            _timer.Elapsed += TimerElapsed;
        }


        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            DateTime dateTimeHora = DateTime.Now;


            if(dateTimeHora.Hour >= 0 && dateTimeHora.Hour <= 4)
            {
                _timer.Enabled = false;
                Ejeuctar_Proceso_Facturacion();
            }
            

        }

        public void Start()
        {
            _timer.Start();
        }


        public void Stop()
        {
            _timer.Stop();
        }


        public void Ejeuctar_Proceso_Facturacion()
        {
            Cls_Ope_Cor_Facturacion_Recibos_Negocios Facturacion_Recibos_Negocio = new Cls_Ope_Cor_Facturacion_Recibos_Negocios(); //variable para la capa de negocios
            BlnEnProceso = true;
            string strFechaActual = DateTime.Now.ToString("dd/MM/yyyy");
            DataTable Dt_Validacion_Estatus = new DataTable();
            DataTable Dt_Total_Predios = new DataTable();
            DataTable Dt_Total_Lecturas = new DataTable();

            bool Rutas_Faltantes = false;

            string mensaje = "";
            try
            {
                // busca un ciclo para facturar por fecha y que no este iniciado
                CicloFacturar = Facturacion_Recibos_Negocio.Obtener_Region_Facturar(strFechaActual);

                if (CicloFacturar.Trim().Length == 0)
                {
                    Utility.Logger("no se encontro un ciclo para facturar");
                    return;
                }

                Facturacion_Recibos_Negocio.P_Region_ID = CicloFacturar;
                Dt_Validacion_Estatus = Facturacion_Recibos_Negocio.Validar_Region_Estatus_Inicio();
                int ID = bitacora_Facturacion_Datos.Obtener_Identificador_Bitacora(Dt_Validacion_Estatus.Rows[0]["No_Facturacion"].ToString().Trim());

                if (ID == 0)
                {
                    Bitacora_Facturacion_ID = bitacora_Facturacion_Datos.Registrar_Bitacora(Dt_Validacion_Estatus.Rows[0]["No_Facturacion"].ToString().Trim());
                }
                else {
                    Bitacora_Facturacion_ID = ID;
                }

                Year = int.Parse(Dt_Validacion_Estatus.Rows[0]["Año"].ToString().Trim());
                Month = int.Parse(Dt_Validacion_Estatus.Rows[0]["Bimestre"].ToString().Trim());
                Facturacion_Recibos_Negocio.P_Año = Year;
                Facturacion_Recibos_Negocio.P_Bimestre = Month;
                Dt_Total_Lecturas = Facturacion_Recibos_Negocio.Consultar_No_Lecturas_Por_Region();
                Dt_Total_Predios = Facturacion_Recibos_Negocio.Consultar_No_Predios_Por_Region();
                mensaje = "+ Algunas rutas no tienen el mismo número de registros de lecturas que cuentas a facturar. ";

                foreach (DataRow Fila in Dt_Total_Predios.Rows)
                {
                    if (Dt_Total_Lecturas.Rows.Count > 0)
                    {
                        DataRow[] Fila_Lectura = Dt_Total_Lecturas.Select("No_Ruta = '" + Fila["No_Ruta"].ToString() + "'");
                        if (Fila_Lectura.Length > 0)
                        {
                            if (Fila["No_Ruta"].ToString() == Fila_Lectura[0]["No_Ruta"].ToString())
                            {
                                bitacora_Facturacion_Datos.Registrar_Descripcion(Bitacora_Facturacion_ID, "Ruta " + Fila["No_Ruta"].ToString() + " --> " + Fila["Cant"].ToString() + " Cuentas --> " + Fila_Lectura[0]["Cant"].ToString() + " Lecturas");

                                if (Fila["Cant"].ToString() != Fila_Lectura[0]["Cant"].ToString())
                                {
                                    Rutas_Faltantes = true;
                                    mensaje += Environment.NewLine + "Ruta " + Fila["No_Ruta"].ToString() + " --> " + Fila["Cant"].ToString() + " Cuentas --> " + Fila_Lectura[0]["Cant"].ToString() + " Lecturas";
                                }
                            }
                        }
                        else
                        {
                            Rutas_Faltantes = true;
                            mensaje += Environment.NewLine + "Ruta " + Fila["No_Ruta"].ToString() + " --> " + Fila["Cant"].ToString() + " Cuentas --> 0 Lecturas";
                            bitacora_Facturacion_Datos.Registrar_Descripcion(Bitacora_Facturacion_ID, "Ruta " + Fila["No_Ruta"].ToString() + " --> " + Fila["Cant"].ToString() + " Cuentas --> 0 Lecturas");
                        }
                    }
                    else
                    {
                        Rutas_Faltantes = true;
                        mensaje += Environment.NewLine + "Ruta " + Fila["No_Ruta"].ToString() + " --> " + Fila["Cant"].ToString() + " Cuentas --> 0 Lecturas";
                        bitacora_Facturacion_Datos.Registrar_Descripcion(Bitacora_Facturacion_ID, "Ruta " + Fila["No_Ruta"].ToString() + " --> " + Fila["Cant"].ToString() + " Cuentas --> 0 Lecturas");
                    }
                }

                if (Rutas_Faltantes)
                    Utility.Logger(mensaje);
                else
                    Utility.Logger("");

                Utility.Logger("DesActivar Pagos APP");
                bitacora_Facturacion_Datos.DesActivarPagoAPP();
                bitacora_Facturacion_Datos.Registrar_Descripcion(Bitacora_Facturacion_ID, "DesActivar Pagos APP");
                mensaje = "";
                //Consultar los predios 
                Dt_Predios_Facturacion = Facturacion_Recibos_Negocio.Consulta_Predios_Region();

                //Verificar si la consulta arrojo resultados
                if (Dt_Predios_Facturacion.Rows.Count > 0)
                {
                    Procesos_Facturacion();
                }
                else
                {
                    Utility.Logger("Activar Pagos APP");
                    bitacora_Facturacion_Datos.ActivarPagoAPP();
                    Utility.Logger("No se encontraron predios a facturar");
                }

            }
            catch (Exception ex)
            {
                Utility.Logger(ex.Message);

                if(Bitacora_Facturacion_ID > 0)
                {
                    try
                    {
                        string mensaje1 = ex.Message.Replace("\"", "");
                        string mensaje2 = mensaje1.Replace("'", "");
                        bitacora_Facturacion_Datos.Registrar_Descripcion(Bitacora_Facturacion_ID, mensaje2);
                        bitacora_Facturacion_Datos.Cambiar_Estatus_Bitacora(Bitacora_Facturacion_ID, "ERROR");
                    }catch(Exception x)
                    {

                    }
                   
                }

                BlnEnProceso = false;
                _timer.Enabled = true;
            }
            finally
            {
                BlnEnProceso = false;
                _timer.Enabled = true;
            }



        }

        public void Procesos_Facturacion()
        {
            Aplicar_Pagos_Pendientes();
            Calcular_Recargos();
            Calcular_Presente_Mes();
            Actualizar_Estatus();
            Aplicar_Pagos_Anticipados();
            bitacora_Facturacion_Datos.Terminar_Bitacora(Bitacora_Facturacion_ID);
            Utility.Logger("Activar Pagos APP");
            bitacora_Facturacion_Datos.ActivarPagoAPP();
            bitacora_Facturacion_Datos.Registrar_Descripcion(Bitacora_Facturacion_ID, "Activar Pagos APP");
        }

        private void Aplicar_Pagos_Pendientes() {

            Cls_Ope_Cor_Facturacion_Recibos_Negocios Cls_Facturacion = new Cls_Ope_Cor_Facturacion_Recibos_Negocios();
            DataTable Dt_Predios = new DataTable();

            Cls_Facturacion.P_Region_ID = CicloFacturar;
            Dt_Predios = Cls_Facturacion.Consultar_Predios_Pagos_Pendientes();
            Utility.Logger("Inicio Pagos Pendientes");
            if (Dt_Predios.Rows.Count > 0) {
                foreach (DataRow fila in Dt_Predios.Rows)
                {
                    Cls_Facturacion.P_Predio_ID = fila["Predio_ID"].ToString();
                    Cls_Facturacion.Aplicar_Pagos_Pendientes();
                    Cls_Facturacion.Calcular_Volumenes_Cobrados_Predio();
                }
            }
            Utility.Logger("Fin Pagos Pendientes");
            bitacora_Facturacion_Datos.Registrar_Descripcion(Bitacora_Facturacion_ID, "Finalizado Aplicación Pagos Pendientes");

        }

        private void Calcular_Recargos()
        {
            Cls_Ope_Cor_Facturacion_Recibos_Negocios Cls_Facturacion = new Cls_Ope_Cor_Facturacion_Recibos_Negocios();
            DataTable Predios_Recargos = new DataTable();
            DataTable Conceptos_Detalle_Recargo = new DataTable();
            DataTable Parametros = new DataTable();
            DataTable Concepto_Redondeo_Anterior = new DataTable();
            DataTable Concepto_Redondeo_Actual = new DataTable();
            DataTable Resultado = new DataTable();
            double importe = 0.0;
            double impuesto = 0.0;
            double total = 0.0;
            double redondeo = 0.0;

            Cls_Facturacion.P_Region_ID = CicloFacturar;

            Parametros = Cls_Facturacion.Obtener_Parametros_Generales_Sistema();
            Concepto_Redondeo_Anterior = Cls_Facturacion.Obtener_Concepto(Parametros.Rows[0]["CONCEPTO_REDONDEO_ANTERIOR"].ToString());
            Concepto_Redondeo_Actual = Cls_Facturacion.Obtener_Concepto(Parametros.Rows[0]["CONCEPTO_REDONDEO"].ToString());
            Predios_Recargos = Cls_Facturacion.Obtener_Facturas_Recargos();


            Utility.Logger("Inicio Calculando Recargos");
            foreach (DataRow Facturas in Predios_Recargos.Rows)
            {

                Cls_Facturacion.P_No_Factura_Recibo = Facturas["No_Factura_Recibo"].ToString();
                Conceptos_Detalle_Recargo = Cls_Facturacion.Obtener_Facturas_Recargos_Detalles(Parametros);

             

                foreach (DataRow Facturas_Detalles in Conceptos_Detalle_Recargo.Rows)
                {

                    importe = 0.0;
                    impuesto = 0.0;
                    total = 0.0;

                    importe = Math.Round(Convert.ToDouble(Facturas_Detalles["Total_Saldo"].ToString()) * (Convert.ToDouble(Parametros.Rows[0]["Porcentaje_Recargos"].ToString()) / 100), 2);

                    if (Convert.ToDouble(Facturas_Detalles["PORCENTAJE_IMPUESTO"]) > 0)
                        impuesto = Math.Round(importe * (Convert.ToDouble(Facturas_Detalles["PORCENTAJE_IMPUESTO"]) / 100) + impuesto, 2);

                    total = Math.Round(importe + impuesto, 2);
                    Cls_Facturacion.Insertar_Detalles_Factura(Facturas["Predio_ID"].ToString(), Facturas["No_Factura_Recibo"].ToString(), Facturas_Detalles["Concepto_ID_Recargo"].ToString(), Facturas_Detalles["Nombre"].ToString(), importe, impuesto, total, Facturas_Detalles["Anio"].ToString(), Facturas_Detalles["Bimestre"].ToString());
                }


                if (Conceptos_Detalle_Recargo.Rows.Count > 0)
                {
                    total = 0.0;
                    impuesto = 0.0;

                    Resultado = Cls_Facturacion.Obtener_Redondeo_Anterior(Facturas["Predio_ID"].ToString(), Conceptos_Detalle_Recargo.Rows[0]["Anio"].ToString(), Conceptos_Detalle_Recargo.Rows[0]["Bimestre"].ToString(), Parametros);
                    if (Resultado.Rows.Count > 0)
                        importe = Convert.ToDouble(Resultado.Rows[0]["Total_Saldo"].ToString()) * -1;
                    else
                        importe = 0.0;

                    total = importe + impuesto;

                    Cls_Facturacion.Insertar_Detalles_Factura(Facturas["Predio_ID"].ToString(), Facturas["No_Factura_Recibo"].ToString(), Concepto_Redondeo_Anterior.Rows[0]["Concepto_ID"].ToString(),
                        Concepto_Redondeo_Anterior.Rows[0]["Nombre"].ToString(), importe, impuesto, total, Conceptos_Detalle_Recargo.Rows[0]["Anio"].ToString(), Conceptos_Detalle_Recargo.Rows[0]["Bimestre"].ToString());

                    Resultado = Cls_Facturacion.Obtener_Saldos_Factura(Facturas["No_Factura_Recibo"].ToString(), Parametros);
                    if (Resultado.Rows.Count > 0)
                    {
                        importe = Convert.ToDouble(Resultado.Rows[0]["Total"].ToString());
                        redondeo = Math.Round(importe);
                        importe = Math.Round(redondeo - importe, 2);
                    }
                    else
                        importe = 0.0;

                    total = importe + impuesto;

                    Cls_Facturacion.Insertar_Detalles_Factura(Facturas["Predio_ID"].ToString(), Facturas["No_Factura_Recibo"].ToString(), Concepto_Redondeo_Actual.Rows[0]["Concepto_ID"].ToString(),
                        Concepto_Redondeo_Actual.Rows[0]["Nombre"].ToString(), importe, impuesto, total, Conceptos_Detalle_Recargo.Rows[0]["Anio"].ToString(), Conceptos_Detalle_Recargo.Rows[0]["Bimestre"].ToString());
                }
            }

            Utility.Logger("Final Calculando Recargos");
            bitacora_Facturacion_Datos.Registrar_Descripcion(Bitacora_Facturacion_ID, "Finalizado Aplicación Recargos");
        }

        private void Calcular_Presente_Mes()
        {
            Cls_Ope_Cor_Facturacion_Recibos_Negocios Cls_Facturacion = new Cls_Ope_Cor_Facturacion_Recibos_Negocios();

            Cls_Facturacion.P_Region_ID = CicloFacturar;
            Cls_Facturacion.P_Año = Year;
            Cls_Facturacion.P_Bimestre = Month;
            Cls_Facturacion.P_Usuario_Creo = "Servicio Facturación";

            Utility.Logger("Inicio Presente Mes");

            foreach (DataRow Fila in Dt_Predios_Facturacion.Rows) {
                Cls_Facturacion.P_Predio_ID = Fila["Predio_ID"].ToString();
                Resultado_Facturacion += Cls_Facturacion.Facturar_Predio(ref mensaje);
            }


            Utility.Logger("Fin Presente Mes");
            bitacora_Facturacion_Datos.Registrar_Descripcion(Bitacora_Facturacion_ID, "Finalizado Cálculo Presente Mes");
        }


        private void Actualizar_Estatus()
        {
            Utility.Logger("Inicio Actualizar Estatus Facturacion");
            Cls_Ope_Cor_Facturacion_Recibos_Negocios Facturacion_Recibos_Negocio = new Cls_Ope_Cor_Facturacion_Recibos_Negocios();
            Facturacion_Recibos_Negocio.P_Region_ID = CicloFacturar;
            Facturacion_Recibos_Negocio.P_Año = Year;
            Facturacion_Recibos_Negocio.P_Bimestre = Month;
            Facturacion_Recibos_Negocio.P_Estatus_Recibo = "CALCULADA";
            Facturacion_Recibos_Negocio.Actualizar_Estatus_Facturacion_Region();
            Utility.Logger("Fin Actualizar Estatus Facturacion");
            bitacora_Facturacion_Datos.Registrar_Descripcion(Bitacora_Facturacion_ID, "Finalizado Actualizar Estatus Facturación");
        }

        private void Aplicar_Pagos_Anticipados()
        {
            Cls_Ope_Cor_Facturacion_Recibos_Negocios Cls_Facturacion = new Cls_Ope_Cor_Facturacion_Recibos_Negocios();
            DataTable Dt_Predios = new DataTable();

            Cls_Facturacion.P_Region_ID = CicloFacturar;
            Dt_Predios = Cls_Facturacion.Consultar_Predios_Pagos_Anticipados();
            Utility.Logger("Inicio Aplicando pagos adelantados");
            if (Dt_Predios.Rows.Count > 0)
            {
               
                foreach (DataRow fila in Dt_Predios.Rows)
                {
                    Cls_Facturacion.P_Predio_ID = fila["Predio_ID"].ToString();
                    Cls_Facturacion.Aplicar_Pagos_Anticipados();
                }
            }

            Utility.Logger("Fin Aplicando pagos adelantados");
            bitacora_Facturacion_Datos.Registrar_Descripcion(Bitacora_Facturacion_ID, "Finalizado Aplicar Pagos Adelantados");
        }

    }
}
