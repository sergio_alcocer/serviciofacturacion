﻿using ServiceFacturacion.Negocio;
using ServiceFacturacion.Util;
using SharpContent.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceFacturacion.Datos
{
   public class Cls_Ope_Cor_Facturacion_Recibos_Datos
    {

        public static string Obtener_Region_Facturar(string strFecha)
        {
            string respuesta = "";

            string Mi_SQL = "SELECT top 1 Region_ID FROM Ope_Cor_Regiones_Pasos_Facturacion WHERE Emision='"+  strFecha + "' and Estatus='INICIADA'";

            respuesta = Convert.ToString(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL));

            return respuesta;


        }

        public static void Actualizar_Estatus_Facturacion_Region(Cls_Ope_Cor_Facturacion_Recibos_Negocios Datos)
        {
            //Declaracion de variables
            SqlTransaction Obj_Transaccion;
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            SqlCommand Obj_Comando = new SqlCommand();

            try
            {
                //Abrir la conexion
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;

                //Ejecutar la consulta
                Obj_Comando.CommandTimeout = 12000;
                Obj_Comando.CommandType = CommandType.StoredProcedure;
                Obj_Comando.CommandText = "SP_Cor_Actualizar_Estatus_Facturacion_Region";
                Obj_Comando.Parameters.Clear();
                Obj_Comando.Parameters.AddWithValue("Region_ID", Datos.P_Region_ID);
                Obj_Comando.Parameters.AddWithValue("Anio", Datos.P_Año);
                Obj_Comando.Parameters.AddWithValue("Mes", Datos.P_Bimestre);
                Obj_Comando.Parameters.AddWithValue("Estatus", Datos.P_Estatus_Recibo);

                //Ejecutar el stored procedure
                Obj_Comando.ExecuteNonQuery();

                //Ejecutar la transaccion y cerrar la conexion
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally {
                if(Obj_Conexion != null)
                {
                    Obj_Conexion.Close();
                }
            }
        }

        public static DataTable Consultar_Predios_Pagos_Anticipados(Cls_Ope_Cor_Facturacion_Recibos_Negocios Datos)
        {
            string Mi_SQL = "";
            DataTable Dt_Recibos_Aplicar;
            try
            {
                Mi_SQL = "SELECT ccp.Predio_ID FROM Cat_Cor_Predios ccp INNER JOIN Ope_Cor_Pagos_Adelantados ocpa " +
                         "ON ccp.RPU = ocpa.RPU WHERE ccp.Estatus != 'CANCELADO' AND ocpa.Estatus = 'PORAPLICAR' " +
                         "AND ccp.Region_ID = '" + Datos.P_Region_ID + "' GROUP BY ccp.Predio_ID";

                Dt_Recibos_Aplicar = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Dt_Recibos_Aplicar;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        public static DataTable Obtener_Saldos_Factura(String No_Factura_Recibo, DataTable Parametros)
        {
            string Mi_SQL = "";
            DataTable Recargo_Anterior_Correcto = new DataTable();

            try
            {
                Mi_SQL = "SELECT No_Movimiento FROM Ope_Cor_Facturacion_Recibos_Detalles" +
                         " WHERE No_Factura_Recibo='" + No_Factura_Recibo + "' AND Estatus IN ('PENDIENTE','PAGADO')" +
                         " AND Concepto_ID='" + Parametros.Rows[0]["CONCEPTO_REDONDEO_ANTERIOR"] + "'" +
                         " ORDER BY No_Movimiento DESC";
                Recargo_Anterior_Correcto = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                Mi_SQL = "SELECT ISNULL(SUM(Importe_Saldo),0) AS Importe, ISNULL(SUM(Impuesto_Saldo),0) AS Impuesto," +
                        " ISNULL(SUM(Total_Saldo), 0) AS Total" +
                        " FROM Ope_Cor_Facturacion_Recibos_Detalles WHERE No_Factura_Recibo='" + No_Factura_Recibo + "'" +
                        " AND Concepto_ID!='" + Parametros.Rows[0]["CONCEPTO_REDONDEO"] + "'";

                if (Recargo_Anterior_Correcto.Rows.Count > 0)
                    Mi_SQL += " AND No_Movimiento!=" + Recargo_Anterior_Correcto.Rows[0]["No_Movimiento"];

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        public static DataTable Obtener_Facturas_Recargos(Cls_Ope_Cor_Facturacion_Recibos_Negocios Datos)
        {
            string Mi_SQL = "";

            try
            {
                Mi_SQL = "SELECT f.Predio_ID, f.No_Factura_Recibo FROM Ope_Cor_Facturacion_Recibos f INNER JOIN Cat_Cor_Predios p" +
                         " ON f.Predio_ID = p.Predio_ID WHERE p.Region_ID = '" + Datos.P_Region_ID + "'" +
                         " AND f.Estatus_Recibo = 'PENDIENTE' AND p.Facturar = 'SI'" +
                         " ORDER BY f.Predio_ID, f.Anio ASC, f.Bimestre ASC";
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }


        public static DataTable Obtener_Concepto(String Concepto_ID)
        {
            string Mi_SQL = "";

            try
            {
                Mi_SQL = "SELECT Concepto_ID, Nombre FROM Cat_Cor_Conceptos_Cobros WHERE Concepto_ID = '" + Concepto_ID + "'";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }


        public static void Aplicar_Pagos_Anticipados(Cls_Ope_Cor_Facturacion_Recibos_Negocios Datos)
        {
            //Declaracion de variables
            SqlTransaction Obj_Transaccion;
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            SqlCommand Obj_Comando = new SqlCommand();

            try
            {
                //Abrir la conexion
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;

                //Ejecutar la consulta
                Obj_Comando.CommandTimeout = 100;
                Obj_Comando.CommandType = CommandType.StoredProcedure;
                Obj_Comando.CommandText = "Sp_Pagos_Anticipados";
                Obj_Comando.Parameters.Clear();
                Obj_Comando.Parameters.AddWithValue("Predio", Datos.P_Predio_ID);

                //Ejecutar el stored procedure
                Obj_Comando.ExecuteNonQuery();

                //Ejecutar la transaccion y cerrar la conexion
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        public static string Facturar_Predio(Cls_Ope_Cor_Facturacion_Recibos_Negocios Datos, ref string mensaje)
        {
            //Declaracion de variables
            string Resultado = string.Empty; //variable para el resultado
            DataTable Dt_Predios = new DataTable(); //tabla para los predios que se van a facturar
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion = null;// new SqlConnection(Cls_Constantes.Str_Conexion);
            SqlCommand Obj_Comando = new SqlCommand();
            SqlConnectionStringBuilder Obj_Conexion_Builder = null;
            string Mi_SQL = "";
            mensaje = "";


            //obj_Conexion_Facturacion_Builder = new SqlConnectionStringBuilder(Cls_Constantes.Str_Conexion);
            //obj_Conexion_Facturacion_Builder.AsynchronousProcessing = true;

            using (Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion))//obj_Conexion_Facturacion_Builder.ConnectionString))
            {
                try
                {
                    //Abrir la conexion
                    Obj_Conexion.Open();
                    Obj_Transaccion = Obj_Conexion.BeginTransaction();
                    Obj_Comando = Obj_Conexion.CreateCommand();
                    Obj_Comando.Transaction = Obj_Transaccion;

                    //Ejecutar la facturacion del predio
                    Obj_Comando.CommandType = CommandType.StoredProcedure;
                    Obj_Comando.CommandText = "SP_Facturar_Predio";
                    Obj_Comando.CommandTimeout = 600;
                    Obj_Comando.Parameters.Clear();
                    Obj_Comando.Parameters.AddWithValue("Predio_ID", Datos.P_Predio_ID);
                    Obj_Comando.Parameters.AddWithValue("Anio", Datos.P_Año);
                    Obj_Comando.Parameters.AddWithValue("Mes", Datos.P_Bimestre);
                    Obj_Comando.Parameters.AddWithValue("Usuario_Creo", Datos.P_Usuario_Creo);

                    //Facturar predio
                    Obj_Comando.ExecuteScalar();

                    //Ejecutar la transaccion
                    Obj_Transaccion.Commit();
                    //resultado = Obj_Comando_Facturar.BeginExecuteNonQuery();

                    //while (!resultado.IsCompleted) { }

                    //Obj_Comando_Facturar.EndExecuteNonQuery(resultado);

                }
                catch (SqlException Ex)
                {
                    Obj_Comando.CommandType = CommandType.Text;
                    Obj_Comando.CommandText = "SELECT No_Cuenta FROM Cat_Cor_Predios WHERE Predio_ID='" + Datos.P_Predio_ID + "'";
                    object No_Cuenta = Obj_Comando.ExecuteScalar();
                    mensaje = " REVISAR el No. de Cuenta " + No_Cuenta;
                    //Obj_Transaccion_Facturar.Rollback();
                    //throw new Exception("Error: " + Ex.Message + " REVISAR el No. de Cuenta " + No_Cuenta);
                }
                catch (DBConcurrencyException Ex)
                {
                    Obj_Comando.CommandType = CommandType.Text;
                    Obj_Comando.CommandText = "SELECT No_Cuenta FROM Cat_Cor_Predios WHERE Predio_ID='" + Datos.P_Predio_ID + "'";
                    object No_Cuenta = Obj_Comando.ExecuteScalar();
                    mensaje = " REVISAR el No. de Cuenta " + No_Cuenta;
                    //Obj_Transaccion_Facturar.Rollback();
                    //throw new Exception("Error: " + Ex.Message + " REVISAR el No. de Cuenta " + No_Cuenta);
                }
                catch (Exception Ex)
                {
                    Obj_Comando.CommandType = CommandType.Text;
                    Obj_Comando.CommandText = "SELECT No_Cuenta FROM Cat_Cor_Predios WHERE Predio_ID='" + Datos.P_Predio_ID + "'";
                    object No_Cuenta = Obj_Comando.ExecuteScalar();
                    mensaje = " REVISAR el No. de Cuenta " + No_Cuenta;
                    //Obj_Transaccion_Facturar.Rollback();
                    //throw new Exception("Error: " + Ex.Message + " REVISAR el No. de Cuenta " + No_Cuenta);
                }
                finally
                {
                    Obj_Conexion.Close();
                }
            }
            //Entregar resultado
            return Resultado;
        }

        public static DataTable Obtener_Redondeo_Anterior(String Predio_ID, String Anio, String Mes, DataTable Parametros)
        {
            string Mi_SQL = "";

            try
            {
                //Se cálcula el recargo anterior correcto
                Mi_SQL = "SELECT TOP 1 Importe_Saldo,Impuesto_Saldo,Total_Saldo" +
                         " FROM Ope_Cor_Facturacion_Recibos_Detalles" +
                         " WHERE No_Factura_Recibo = (" +
                         "      SELECT TOP 1 No_Factura_Recibo" +
                         "      FROM Ope_Cor_Facturacion_Recibos" +
                         "      WHERE CAST('01/' + CAST(Bimestre AS VARCHAR) + '/' + CAST(Anio AS VARCHAR) AS DATE) <" +
                         "          CAST('01/' + CAST(" + Mes + " AS VARCHAR) + '/' + CAST(" + Anio + " AS VARCHAR) AS DATE)" +
                         "          AND Predio_ID = '" + Predio_ID + "'" +
                         "      ORDER BY Anio DESC,Bimestre DESC)" +
                         " AND Concepto_ID = '" + Parametros.Rows[0]["CONCEPTO_REDONDEO"] + "'" +
                         " ORDER BY No_Movimiento DESC";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        public static void Insertar_Detalles_Factura(String Predio_ID, String No_Factura_Recibo, string Concepto_ID, string Nombre_Concepto, double Importe, double Impuesto, double Total, string Anio, string Mes)
        {
            String Mi_SQL = String.Empty;
            SqlConnection Obj_Coneccion = new SqlConnection(Cls_Constantes.Str_Conexion);
            SqlCommand Obj_Comando = null;
            SqlTransaction Obj_Transaccion = null;

            try
            {
                Obj_Coneccion.Open();
                Obj_Comando = Obj_Coneccion.CreateCommand();
                Obj_Transaccion = Obj_Coneccion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;

                Mi_SQL = @"INSERT INTO Ope_Cor_Facturacion_Recibos_Detalles (No_Factura_Recibo,Concepto_ID,Concepto,Importe,Impuesto,Total," +
                        "Importe_Abonado,Impuesto_Abonado,Total_Abonado,Importe_Saldo,Impuesto_Saldo,Total_Saldo,Anio,Bimestre,Estatus,Predio_ID)" +
                        "VALUES (@No_Factura_Recibo,@Concepto_ID,@Nombre_Concepto,@Importe,@Impuesto,@Total," +
                        "0,0,0,@Importe,@Impuesto,@Total,@Anio,@Mes,'PREFACTURA',@Predio_ID)";

                Obj_Comando.Parameters.AddWithValue("@No_Factura_Recibo", No_Factura_Recibo);
                Obj_Comando.Parameters.AddWithValue("@Concepto_ID", Concepto_ID);
                Obj_Comando.Parameters.AddWithValue("@Nombre_Concepto", Nombre_Concepto);
                Obj_Comando.Parameters.AddWithValue("@Importe", Importe);
                Obj_Comando.Parameters.AddWithValue("@Impuesto", Impuesto);
                Obj_Comando.Parameters.AddWithValue("@Total", Total);
                Obj_Comando.Parameters.AddWithValue("@Anio", Convert.ToDouble(Anio));
                Obj_Comando.Parameters.AddWithValue("@Mes", Convert.ToDouble(Mes));
                Obj_Comando.Parameters.AddWithValue("@Predio_ID", Predio_ID);

                Obj_Comando.CommandTimeout = 600;
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.CommandType = CommandType.Text;
                Obj_Comando.ExecuteNonQuery();

                Obj_Transaccion.Commit();
                Obj_Coneccion.Close();
            }
            catch (SqlException Ex)
            {
                Obj_Transaccion.Rollback();
                //return false;
                throw new Exception("Error: " + Ex.Message);

            }
            catch (DBConcurrencyException Ex)
            {
                Obj_Transaccion.Rollback();
                //return false;
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                Obj_Transaccion.Rollback();
                //return false;
                throw new Exception("Error: " + Ex.Message);
            }
        }

        public static DataTable Obtener_Facturas_Recargos_Detalles(Cls_Ope_Cor_Facturacion_Recibos_Negocios Datos, DataTable Parametros)
        {
            string Mi_SQL = "";

            try
            {
                Mi_SQL = "SELECT c.Concepto_ID,ISNULL(c.Concepto_Recargo_ID, c.Concepto_ID) AS Concepto_ID_Recargo,(SELECT f.Nombre FROM Cat_Cor_Conceptos_Cobros f" +
                         " WHERE f.Concepto_ID = ISNULL(c.Concepto_Recargo_ID, c.Concepto_ID)) AS Nombre,d.Importe_Saldo as Total_Saldo" +
                         ",(SELECT i.PORCENTAJE_IMPUESTO FROM Cat_Cor_Conceptos_Cobros f INNER JOIN CAT_COM_IMPUESTOS i ON f.impuesto_id = i.IMPUESTO_ID" +
                         " WHERE f.Concepto_ID = ISNULL(c.Concepto_Recargo_ID, c.Concepto_ID)) AS PORCENTAJE_IMPUESTO,d.Anio,d.Bimestre" +
                         " FROM Ope_Cor_Facturacion_Recibos_Detalles d INNER JOIN Cat_Cor_Conceptos_Cobros c ON d.Concepto_ID = c.Concepto_ID" +
                         " WHERE d.Estatus = 'PENDIENTE' AND d.No_Factura_Recibo = '" + Datos.P_No_Factura_Recibo + "' AND" +
                         " (d.Concepto_ID = '" + Parametros.Rows[0]["CONCEPTO_AGUA"] + "' OR d.Concepto_ID = '" + Parametros.Rows[0]["Concepto_Agua_Comercial"] + "'" +
                         " OR d.Concepto_ID = '" + Parametros.Rows[0]["CONCEPTO_DRENAJE"] + "' OR d.Concepto_ID = '" + Parametros.Rows[0]["CONCEPTO_SANAMIENTO"] + "'" +
                         " OR d.Concepto_ID = '" + Parametros.Rows[0]["Concepto_Rezago_Agua_Id"] + "' OR d.Concepto_ID = '" + Parametros.Rows[0]["Concepto_Rezago_Drenaje_Id"] + "'" +
                         " OR d.Concepto_ID = '" + Parametros.Rows[0]["Concepto_Rezago_Saneamiento_Id"] + "')";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        public static DataTable Obtener_Parametros_Generales_Sistema()//SqlCommand Obj_Comando)
        {
            string Mi_SQL = "";
            //DataTable Resultado = new DataTable();
            //SqlDataReader Dr_Lector;

            try
            {
                Mi_SQL = "SELECT Porcentaje_IVA,Porcentaje_Recargos,CONCEPTO_AGUA,Concepto_Agua_Comercial,CONCEPTO_DRENAJE," +
                         "CONCEPTO_SANAMIENTO,Concepto_Bomberos,Concepto_Cruz_Roja,CONCEPTO_REDONDEO_ANTERIOR,CONCEPTO_REDONDEO," +
                         "Concepto_Recargo_Agua_Id,Concepto_Recargo_Drenaje_Id,Concepto_Recargo_Saneamiento_Id,Concepto_Rezago_Agua_Id," +
                         "Concepto_Rezago_Drenaje_Id,Concepto_Rezago_Saneamiento_Id FROM Cat_Cor_Parametros";
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                //Obj_Comando.CommandText = Mi_SQL;
                //Obj_Comando.CommandType = CommandType.Text;
                //Dr_Lector = Obj_Comando.ExecuteReader();
                //Resultado.Load(Dr_Lector);
                //return Resultado;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        public static void Calcular_Volumenes_Cobrados_Predio(Cls_Ope_Cor_Facturacion_Recibos_Negocios Datos)
        {
            //Declaracion de variables
            SqlTransaction Obj_Transaccion;
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            SqlCommand Obj_Comando = new SqlCommand();

            try
            {
                //Abrir la conexion
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;

                //Ejecutar la consulta
                Obj_Comando.CommandTimeout = 100;
                Obj_Comando.CommandType = CommandType.StoredProcedure;
                Obj_Comando.CommandText = "Sp_Calcular_Volumenes_Cobrados_Predio";
                Obj_Comando.Parameters.Clear();
                Obj_Comando.Parameters.AddWithValue("predio_id", Int64.Parse(Datos.P_Predio_ID));

                //Ejecutar el stored procedure
                Obj_Comando.ExecuteNonQuery();

                //Ejecutar la transaccion y cerrar la conexion
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        public static void Aplicar_Pagos_Pendientes(Cls_Ope_Cor_Facturacion_Recibos_Negocios Datos)
        {
            //Declaracion de variables
            SqlTransaction Obj_Transaccion;
            SqlConnection Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            SqlCommand Obj_Comando = new SqlCommand();

            try
            {
                //Abrir la conexion
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;

                //Ejecutar la consulta
                Obj_Comando.CommandTimeout = 60 * 10;
                Obj_Comando.CommandType = CommandType.StoredProcedure;
                Obj_Comando.CommandText = "Sp_Pagos_Pendientes";
                Obj_Comando.Parameters.Clear();
                Obj_Comando.Parameters.AddWithValue("Predio", Datos.P_Predio_ID);

                //Ejecutar el stored procedure
                Obj_Comando.ExecuteNonQuery();

                //Ejecutar la transaccion y cerrar la conexion
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        public static DataTable Consultar_Predios_Pagos_Pendientes(Cls_Ope_Cor_Facturacion_Recibos_Negocios Datos)
        {
            string Mi_SQL = "";
            DataTable Dt_Recibos_Aplicar;
            try
            {
                Mi_SQL = "SELECT ccp.Predio_ID FROM Cat_Cor_Predios ccp INNER JOIN Ope_Cor_Caj_Recibos_Cobros occrc " +
                         "ON ccp.RPU = occrc.RPU WHERE ccp.Estatus != 'CANCELADO' AND occrc.estado_recibo = 'PORAPLICAR' " +
                         "AND ccp.Region_ID = '" + Datos.P_Region_ID + "' GROUP BY ccp.Predio_ID";

                Dt_Recibos_Aplicar = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Dt_Recibos_Aplicar;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }


        public static DataTable Consulta_Predios_Region(Cls_Ope_Cor_Facturacion_Recibos_Negocios Datos)
        {
            //Declaracion de variables
            DataTable Dt_Resultado = new DataTable(); //tabla para el resultado
            SqlConnection Obj_Conexion = null;
            SqlCommand Obj_Comando = null;
            SqlDataReader Dr_Lector;
            string Mi_SQL = "";

            try
            {
                //Asignar conexion
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Conexion.Open();
                Obj_Comando = Obj_Conexion.CreateCommand();

                Mi_SQL = "DELETE FROM Ope_Cor_Facturacion_Recibos_Detalles WHERE Estatus='PREFACTURA' AND Predio_ID IN" +
                        " (SELECT Predio_ID FROM Cat_Cor_Predios WHERE Region_ID='" + Datos.P_Region_ID + "')";
                Obj_Comando.CommandType = CommandType.Text;
                Obj_Comando.CommandTimeout = 600;
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                Mi_SQL = "DELETE FROM Ope_Cor_Facturacion_Recibos WHERE Estatus_Recibo='PREFACTURA' AND Predio_ID IN" +
                        " (SELECT Predio_ID FROM Cat_Cor_Predios WHERE Region_ID='" + Datos.P_Region_ID + "')";
                Obj_Comando.CommandType = CommandType.Text;
                Obj_Comando.CommandTimeout = 600;
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                Obj_Comando.CommandText = "SP_Consultar_Predio_Facturar";
                Obj_Comando.CommandType = CommandType.StoredProcedure;
                Obj_Comando.CommandTimeout = 600;
                Obj_Comando.Parameters.Add("@Region_ID", SqlDbType.VarChar, 5).Value = Datos.P_Region_ID;
                Dr_Lector = Obj_Comando.ExecuteReader();
                Dt_Resultado.Load(Dr_Lector);

                //Cerrar conexion
                Dr_Lector.Close();
                //Entregar resultado
                return Dt_Resultado;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                Obj_Conexion.Close();
            }
        }


        public static DataTable Validar_Region_Estatus_Inicio(Cls_Ope_Cor_Facturacion_Recibos_Negocios Datos)
        {
            //Declaracion de variables
            String Mi_SQL = ""; //variable para la consulta de SQL
            DataTable Dt_Resultado = new DataTable(); //tabla para el resultado

            try
            {
                //Asignar la consulta
                Mi_SQL = "SELECT No_Facturacion, Estatus, Periodo_Facturacion , Vencimiento As Fecha_Vencimiento, Mes,Emision As Fecha_Emision, Año, Bimestre";
                Mi_SQL = Mi_SQL + " FROM Ope_Cor_Regiones_Pasos_Facturacion";
                Mi_SQL = Mi_SQL + " WHERE Region_ID = '" + Datos.P_Region_ID + "'";

                //Filtro por la fecha del periodo de facturacion
                if (!string.IsNullOrEmpty(Datos.P_Periodo_Facturacion))
                    Mi_SQL = Mi_SQL + " AND Periodo_Facturacion = '" + Datos.P_Periodo_Facturacion + "'";

                Mi_SQL += "ORDER BY Emision DESC";

                //Ejecutar consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Dt_Resultado;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }



        public static DataTable Consultar_No_Lecturas_Por_Region(Cls_Ope_Cor_Facturacion_Recibos_Negocios Datos)
        {
            string Mi_SQL = "";

            try
            {
                Mi_SQL = "SELECT r.No_Ruta, COUNT(*) AS Cant" +
                        " FROM Cat_Cor_Medidores_Lecturas_Detalles l INNER JOIN" +
                        " Cat_Cor_Predios p ON l.Predio_ID=p.Predio_ID INNER JOIN" +
                        " Cat_Cor_Rutas_Reparto r ON r.Ruta_Reparto_ID=p.Ruta_Reparto_ID" +
                        " WHERE p.Region_ID='" + Datos.P_Region_ID + "'" +
                        " AND p.Facturar='SI'" +
                        " AND l.Anio=" + Datos.P_Año +
                        " AND l.Bimestre=" + Datos.P_Bimestre +
                        " GROUP BY r.No_Ruta" +
                        " ORDER BY r.No_Ruta";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        public static DataTable Consultar_No_Predios_Por_Region(Cls_Ope_Cor_Facturacion_Recibos_Negocios Datos)
        {
            string Mi_SQL = "";

            try
            {
                Mi_SQL = "SELECT r.No_Ruta, COUNT(*) AS Cant" +
                        " FROM Cat_Cor_Predios p INNER JOIN" +
                        " Cat_Cor_Rutas_Reparto r ON r.Ruta_Reparto_ID=p.Ruta_Reparto_ID" +
                        " WHERE p.Region_ID='" + Datos.P_Region_ID + "'" +
                        " AND p.Facturar='SI'" +
                        " GROUP BY r.No_Ruta" +
                        " ORDER BY r.No_Ruta";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }


    }
}
