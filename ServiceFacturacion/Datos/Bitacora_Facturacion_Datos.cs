﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using ServiceFacturacion.Util;

namespace ServiceFacturacion.Datos
{
   public class Bitacora_Facturacion_Datos
    {

        public string str_sql;


        public void DesActivarPagoAPP() {

            using (SqlConnection connection = new SqlConnection(Cls_Constantes.Str_Conexion))
            {
                connection.Open();

                using (SqlCommand command = connection.CreateCommand())
                {

                    str_sql = "UPDATE APP_Configuracion SET Servicio_PE_Moviles='NO'";
                    command.CommandText = str_sql;
                    command.ExecuteNonQuery();
                }
            }

        }

        public void ActivarPagoAPP() {

            using (SqlConnection connection = new SqlConnection(Cls_Constantes.Str_Conexion))
            {
                connection.Open();

                using (SqlCommand command = connection.CreateCommand()) {

                    str_sql = "UPDATE APP_Configuracion SET Servicio_PE_Moviles='SI'";
                    command.CommandText = str_sql;
                    command.ExecuteNonQuery();
                }
            }

        }

        public void Registrar_Descripcion(int Bitacora_Facturacion_ID, string Descripcion)
        {

            using (SqlConnection conexion = new SqlConnection(Cls_Constantes.Str_Conexion))
            {
                conexion.Open();

                using (SqlCommand comando = conexion.CreateCommand())
                {
                    str_sql = "INSERT INTO Bitacora_Facturacion_Detalle(Bitacora_Facturacion_ID,Descripcion) VALUES(@id,@descripcion)";
                    comando.CommandText = str_sql;
                    comando.CommandType = CommandType.Text;
                    comando.Parameters.Add(new SqlParameter("@id", Bitacora_Facturacion_ID));
                    comando.Parameters.Add(new SqlParameter("@descripcion", Descripcion));
                    comando.ExecuteNonQuery();

                }
            }

        }


        public int Obtener_Identificador_Bitacora(string No_Facturacion)
        {
            int respuesta = 0;

            using (SqlConnection conexion = new SqlConnection(Cls_Constantes.Str_Conexion))
            {
                conexion.Open();

                using (SqlCommand comando = conexion.CreateCommand())
                {
                    str_sql = "SELECT Bitacora_Facturacion_ID FROM Bitacora_Facturacion WHERE No_Facturacion=@nofacturacion ";

                    comando.CommandText = str_sql;
                    comando.CommandType = CommandType.Text;
                    comando.Parameters.Add(new SqlParameter("@nofacturacion", No_Facturacion));
                    respuesta = Convert.ToInt32(comando.ExecuteScalar());
                }
            }

            return respuesta;
        }

        public void Terminar_Bitacora(int Bitacora_Facturacion_ID) {


            using (SqlConnection conexion = new SqlConnection(Cls_Constantes.Str_Conexion))
            {
                conexion.Open();

                using (SqlCommand comando = conexion.CreateCommand())
                {

                    str_sql = "UPDATE Bitacora_Facturacion SET Estatus='TERMINADO', Fecha_Final=GETDATE() WHERE Bitacora_Facturacion_ID=@ID";
                    comando.CommandText = str_sql;
                    comando.CommandType = CommandType.Text;

                    comando.Parameters.Add(new SqlParameter("@ID", Bitacora_Facturacion_ID));
                    comando.ExecuteNonQuery();
                }
            }

        }




        public void Cambiar_Estatus_Bitacora(int Bitacora_Facturacion_ID, string Estatus)
        {


            using (SqlConnection conexion = new SqlConnection(Cls_Constantes.Str_Conexion))
            {
                conexion.Open();

                using (SqlCommand comando = conexion.CreateCommand())
                {

                    str_sql = "UPDATE Bitacora_Facturacion SET Estatus='" + Estatus + "', Fecha_Final=GETDATE() WHERE Bitacora_Facturacion_ID=@ID";
                    comando.CommandText = str_sql;
                    comando.CommandType = CommandType.Text;

                    comando.Parameters.Add(new SqlParameter("@ID", Bitacora_Facturacion_ID));
                    comando.ExecuteNonQuery();
                }
            }

        }

        public int Registrar_Bitacora(string No_Facturacion)
        {
            int respuesta = 0;

            using (SqlConnection conexion = new SqlConnection(Cls_Constantes.Str_Conexion))
            {
                conexion.Open();

                using (SqlCommand comando = conexion.CreateCommand()) {

                    str_sql = "INSERT into Bitacora_Facturacion(No_Facturacion,Fecha_Inicio,Estatus) VALUES (@nofacturacion,getdate(),'INICIADO') ; SELECT CAST(scope_identity() AS int);";
                    comando.CommandText = str_sql;
                    comando.CommandType = CommandType.Text;

                    comando.Parameters.Add(new SqlParameter("@nofacturacion", No_Facturacion));
                    respuesta = Convert.ToInt32(comando.ExecuteScalar());

                   
                }


            }


            return respuesta;
        }

    }
}
