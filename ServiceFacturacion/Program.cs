﻿using ServiceFacturacion.Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace ServiceFacturacion
{
    class Program
    {
        static void Main(string[] args)
        {
            // RespositorioEscritorio\serviciofacturacion\ServiceFacturacion\bin\Debug
            //ServiceFacturacion.exe install start
            //ServiceFacturacion.exe uninstall

            var exitCode = HostFactory.Run(x =>
            {

                x.Service<FacturaServicio>(s =>
                {
                    s.ConstructUsing(h => new FacturaServicio());
                    s.WhenStarted(h => h.Start());
                    s.WhenStopped(h => h.Stop());

                });

                x.RunAsLocalSystem();

                x.SetServiceName("FacturacionService");
                x.SetDisplayName("Servicio Facturación******************************************************");
                x.SetDescription("Factura los ciclos del SIMAPAG");
            });


            int exitCodeValue = (int)Convert.ChangeType(exitCode, exitCode.GetTypeCode());
            Environment.ExitCode = exitCodeValue;


        }
    }
}
