﻿using ServiceFacturacion.Datos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceFacturacion.Negocio
{
  public  class Cls_Ope_Cor_Facturacion_Recibos_Negocios
    {
        #region Variables Publicas
        public String P_Region_ID { get; set; }
        public String P_Predio_ID { get; set; }
        public int P_Zona_Factura { get; set; }
        public String P_Convenio_ID { get; set; }
        public String P_Otros_Cargos_ID { get; set; }
        public String P_Recargo_ID { get; set; }
        public String P_Rezago_ID { get; set; }
        public String P_No_Factura_Recibo { get; set; }
        public String P_Folio_Inicial { get; set; }
        public String P_Folio_Final { get; set; }
        public String P_No_Cuenta { get; set; }
        public String P_Medidor_Detalle_ID { get; set; }
        public String P_Sector_ID { get; set; }
        public String P_Usuario_ID { get; set; }
        public String P_Medidor_ID { get; set; }
        public String P_Tarifa_ID { get; set; }
        public String P_No_Convenio { get; set; }
        public String P_Codigo_Barras { get; set; }
        public Double P_Lectura_Anterior { get; set; }
        public Double P_Lectura_Actual { get; set; }
        public Double P_Consumo { get; set; }
        public Double P_Consumo_Rango_Final { get; set; }
        public Double P_Cuota_Base { get; set; }
        public Double P_Cuota_Consumo { get; set; }
        public Double P_Precio_Metro_Cubico { get; set; }
        public String P_Fecha_Inicio_Periodo { get; set; }
        public String P_Fecha_Termino_Periodo { get; set; }
        public string P_Fecha_Limite_Pago { get; set; }
        public string P_Fecha_Emision { get; set; }
        public String P_Periodo_Facturacion { get; set; }
        public Double P_Tasa_IVA { get; set; }
        public Double P_Total_Importe { get; set; }
        public Double P_Total_IVA { get; set; }
        public Double P_Total_Pagar { get; set; }
        public Double P_Total_Abono { get; set; }
        public Double P_Saldo { get; set; }
        public String P_Estatus_Recibo { get; set; }
        public String P_Estatus_Impresion { get; set; }
        public DateTime P_Fecha_Imprimio { get; set; }
        public String P_Usuario_Imprimio { get; set; }
        public String P_Estatus_Reimpresion { get; set; }
        public DateTime P_Fecha_Reimprimio { get; set; }
        public String P_Usuario_Reimprimio { get; set; }
        public Double P_Cantidad_Reimpresiones { get; set; }
        public String P_Comentarios { get; set; }
        public String P_Usuario_Creo { get; set; }
        public DateTime P_Fecha_Creo { get; set; }
        public String P_Usuario_Modifico { get; set; }
        public DateTime P_Fecha_Modifico { get; set; }
        public DataTable P_Dt_Facturacion_Detalles { get; set; }
        public DataTable P_Dt_Facturacion { get; set; }
        public String P_Concepto_ID { get; set; }
        public String P_Concepto { get; set; }
        public Double P_Importe { get; set; }
        public Double P_Impuesto { get; set; }
        public Double P_Total { get; set; }
        public String P_Mes { get; set; }
        public Int32 P_Año { get; set; }
        public Int32 P_Total_M3 { get; set; }
        public Int32 P_Saldo_M3 { get; set; }
        public string P_Pago_Anual { get; set; }
        public string P_Tipo_Cuota { get; set; }
        public string P_Nombre_Recibo { get; set; }
        public double P_Rezago { get; set; }
        public int P_Rango_Final { get; set; }
        public DataRow P_Parametro_Conceptos { get; set; }
        public string P_Fecha_Corte { get; set; }
        public int P_Bimestre { get; set; }
        #endregion


        #region Metodos
        public DataTable Validar_Region_Estatus_Inicio() { return Cls_Ope_Cor_Facturacion_Recibos_Datos.Validar_Region_Estatus_Inicio(this); }
        public DataTable Consultar_No_Lecturas_Por_Region() { return Cls_Ope_Cor_Facturacion_Recibos_Datos.Consultar_No_Lecturas_Por_Region(this); }
        public DataTable Consultar_No_Predios_Por_Region() { return Cls_Ope_Cor_Facturacion_Recibos_Datos.Consultar_No_Predios_Por_Region(this); }
        public DataTable Consulta_Predios_Region() { return Cls_Ope_Cor_Facturacion_Recibos_Datos.Consulta_Predios_Region(this); }
        public DataTable Consultar_Predios_Pagos_Pendientes() { return Cls_Ope_Cor_Facturacion_Recibos_Datos.Consultar_Predios_Pagos_Pendientes(this); }
        public void Aplicar_Pagos_Pendientes() { Cls_Ope_Cor_Facturacion_Recibos_Datos.Aplicar_Pagos_Pendientes(this); }
        public void Calcular_Volumenes_Cobrados_Predio() { Cls_Ope_Cor_Facturacion_Recibos_Datos.Calcular_Volumenes_Cobrados_Predio(this); }
        public DataTable Obtener_Parametros_Generales_Sistema() { return Cls_Ope_Cor_Facturacion_Recibos_Datos.Obtener_Parametros_Generales_Sistema(); }
        public DataTable Obtener_Facturas_Recargos_Detalles(DataTable Parametros) { return Cls_Ope_Cor_Facturacion_Recibos_Datos.Obtener_Facturas_Recargos_Detalles(this, Parametros); }
        public void Insertar_Detalles_Factura(String Predio_ID, String No_Factura_Recibo, String Concepto_ID, String Nombre_Concepto, double Importe, double Impuesto, double Total, String Anio, String Mes)
        { Cls_Ope_Cor_Facturacion_Recibos_Datos.Insertar_Detalles_Factura(Predio_ID, No_Factura_Recibo, Concepto_ID, Nombre_Concepto, Importe, Impuesto, Total, Anio, Mes); }
        public DataTable Obtener_Redondeo_Anterior(String Predio_ID, String Anio, String Mes, DataTable Parametros) { return Cls_Ope_Cor_Facturacion_Recibos_Datos.Obtener_Redondeo_Anterior(Predio_ID, Anio, Mes, Parametros); }
        public string Facturar_Predio(ref string Mensaje) { Cls_Ope_Cor_Facturacion_Recibos_Datos.Facturar_Predio(this, ref Mensaje); return ""; }
        public void Aplicar_Pagos_Anticipados() { Cls_Ope_Cor_Facturacion_Recibos_Datos.Aplicar_Pagos_Anticipados(this); }
        public DataTable Obtener_Concepto(String Concepto_ID) { return Cls_Ope_Cor_Facturacion_Recibos_Datos.Obtener_Concepto(Concepto_ID); }
        public DataTable Obtener_Facturas_Recargos() { return Cls_Ope_Cor_Facturacion_Recibos_Datos.Obtener_Facturas_Recargos(this); }
        public DataTable Obtener_Saldos_Factura(String No_Factura_Recibo, DataTable Parametros) { return Cls_Ope_Cor_Facturacion_Recibos_Datos.Obtener_Saldos_Factura(No_Factura_Recibo, Parametros); }
        public DataTable Consultar_Predios_Pagos_Anticipados() { return Cls_Ope_Cor_Facturacion_Recibos_Datos.Consultar_Predios_Pagos_Anticipados(this); }
        public void Actualizar_Estatus_Facturacion_Region() { Cls_Ope_Cor_Facturacion_Recibos_Datos.Actualizar_Estatus_Facturacion_Region(this); }
        public string Obtener_Region_Facturar(string strFecha)
        {
            return Cls_Ope_Cor_Facturacion_Recibos_Datos.Obtener_Region_Facturar(strFecha);
        }

        #endregion
    }
}
